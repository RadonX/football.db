# http://dev.mysql.com/doc/refman/5.6/en/load-data.html

use football;

# -------- 1. CountryName -------- 

LOAD DATA INFILE "/Users/radon/GIT/soccer/data/fifacode.csv" 
    REPLACE INTO TABLE CountryName FIELDS 
    TERMINATED BY ',' 
    IGNORE 1 LINES (ccode, country);

LOAD DATA INFILE "/Users/radon/GIT/soccer/data/competition.csv" 
    REPLACE INTO TABLE Competition FIELDS 
    TERMINATED BY ',' 
    (ctype, cyear);


# -------- 2. country->ccode, pname->pid -------- 

DELIMITER $$
CREATE FUNCTION countryCode(_country VARCHAR(40) )
RETURNS CHAR(4) #READS SQL DATA
BEGIN
DECLARE _ccode CHAR(4);
#DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET rate_var = 0; 
    SELECT ccode INTO _ccode FROM CountryName 
    WHERE country = _country;
    RETURN _ccode;
END; $$
DELIMITER ;


DELIMITER $$
CREATE FUNCTION playerID(_pname VARCHAR(60) ) # 模糊查询？
RETURNS INT UNSIGNED 
BEGIN
DECLARE _pid INT UNSIGNED;
    SELECT pid INTO _pid FROM Player 
    WHERE pname = _pname
    LIMIT 1; # assume UNIQUE pname, which may be true. should add pdob!!!
    RETURN _pid; 
END; $$
DELIMITER ;


DELIMITER $$
CREATE FUNCTION ncID(_ccode CHAR(4), _ctype VARCHAR(16), _cyear YEAR(4))
RETURNS INT UNSIGNED 
BEGIN
DECLARE _ncid INT UNSIGNED;
    SELECT ncid INTO _ncid FROM ntPlaysIn 
    WHERE ccode = _ccode AND ctype = _ctype AND cyear = _cyear;
    RETURN _ncid; 
END; $$
DELIMITER ;

# -------- 3. NationalTeam & Player-------- 

# loadcsv.ins_NationalTeam
# loadcsv.ins_Player
# loadcsv.ins_Club
# loadcsv.ins_pnc (ntPlaysIn, playsForCup)
# ...


