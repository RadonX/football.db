# ---------- basic -------------
# necessary for importing csv files with python 


DELIMITER $$
CREATE FUNCTION countryCode(_country VARCHAR(40) )
RETURNS CHAR(4) #READS SQL DATA
BEGIN
DECLARE _ccode CHAR(4);
#DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET rate_var = 0; 
    SELECT ccode INTO _ccode FROM CountryName 
    WHERE country = _country;
    RETURN _ccode;
END; $$
DELIMITER ;


DELIMITER $$
CREATE FUNCTION playerID(_pname VARCHAR(60) ) # 模糊查询？
RETURNS INT UNSIGNED 
BEGIN
DECLARE _pid INT UNSIGNED;
    SELECT pid INTO _pid FROM Player 
    WHERE pname = _pname
    LIMIT 1; # assume UNIQUE pname, which may be true. should add pdob!!!
    RETURN _pid; 
END; $$
DELIMITER ;


DELIMITER $$
CREATE FUNCTION ncID(_ccode CHAR(4), _ctype VARCHAR(16), _cyear YEAR(4))
RETURNS INT UNSIGNED 
BEGIN
DECLARE _ncid INT UNSIGNED;
    SELECT ncid INTO _ncid FROM ntPlaysIn 
    WHERE ccode = _ccode AND ctype = _ctype AND cyear = _cyear;
    RETURN _ncid; 
END; $$
DELIMITER ;

# -----

DROP procedure IF EXISTS `get_CountryName`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_CountryName`(
    IN ccode_ CHAR(4)
)
BEGIN
    SELECT country FROM CountryName
    WHERE ccode = ccode_;
END$$
DELIMITER ;


DROP procedure IF EXISTS `get_NationalTeam`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_NationalTeam`(
    IN confederation_ VARCHAR(10)
)
BEGIN
    SELECT tmp.ccode, tmp.country
    FROM(
        SELECT N.ccode, country, confederation
        FROM NationalTeam N INNER JOIN CountryName C
        ON N.ccode = C.ccode
    ) AS tmp
    WHERE confederation = confederation_;
END$$
DELIMITER ;


# ---------------------

## procedure that takes in ccode and returns country, confederation and captain's name

DROP FUNCTION IF EXISTS cap_pid;
DELIMITER $$
CREATE FUNCTION cap_pid(_ncid INT(4))
RETURNS INT UNSIGNED
BEGIN
DECLARE _cap_pid INT UNSIGNED;
SELECT pid INTO _cap_pid FROM playsForCup P
WHERE P.ncid = _ncid AND P.iscap = 1 ;
RETURN _cap_pid;
END; $$
DELIMITER ;

# pass a player's pid and return a player's name
DROP FUNCTION IF EXISTS p_name;
DELIMITER $$
CREATE FUNCTION p_name(_pid INT UNSIGNED)
RETURNS VARCHAR(60)
BEGIN
DECLARE _pname VARCHAR(60);
SELECT pname INTO _pname FROM Player P
WHERE P.pid = _pid;
RETURN _pname;
END; $$
DELIMITER ;

DROP procedure IF EXISTS get_countryInfo;
DELIMITER $$
CREATE DEFINER=root@localhost PROCEDURE get_countryInfo(
IN ccode_ CHAR(4)
)
BEGIN
DECLARE cap_pid INT UNSIGNED;
DECLARE n_c_id INT UNSIGNED;
DECLARE cap_name VARCHAR(60);

SET n_c_id = ncID(ccode_, "WC", 2014);
SET cap_pid = cap_pid(n_c_id);
SET cap_name = p_name(cap_pid);

SELECT country, confederation, pname From CountryName C, NationalTeam N, Player P
WHERE C.ccode = N.ccode AND C.ccode = P.pnationality AND C.ccode = ccode_ AND P.pname = cap_name
LIMIT 1;

END$$
DELIMITER ;


# ----------fun-------------

##clubs' contribution

DROP procedure IF EXISTS get_clubContr;
DELIMITER $$
CREATE DEFINER=root@localhost PROCEDURE get_clubContr(
IN _year INT UNSIGNED
)
BEGIN
SELECT P.clname, COUNT(*)
FROM Player P, playsForCup PFC, ntplaysin NTP
WHERE PFC.pid = P.pid AND PFC.ncid = NTP.ncid AND NTP.cyear = _year
GROUP BY P.clname;

END$$
DELIMITER ;

##countries' contribution

DROP procedure IF EXISTS get_countContr;
DELIMITER $$
CREATE DEFINER=root@localhost PROCEDURE get_countContr(
IN _year INT UNSIGNED
)
BEGIN
SELECT P.pnationality, COUNT(*)
FROM Player P, playsForCup PFC, ntplaysin NTP
WHERE PFC.pid = P.pid AND PFC.ncid = NTP.ncid AND NTP.cyear = _year
GROUP BY P.pnationality;

END$$
DELIMITER ;


#一个 League 在某届世界杯中贡献的球员数

DELIMITER $$
CREATE DEFINER=root@localhost PROCEDURE get_LeagueContr(
IN _year YEAR
)
BEGIN
SELECT C.league, COUNT(*)
FROM Club C, Player P, playsForCup PFC, ntplaysin NTP
WHERE C.clname = P.clname AND PFC.pid = P.pid AND PFC.ncid = NTP.ncid AND NTP.cyear = _year
GROUP BY C.league;
END$$
DELIMITER ;


#一个  confederation 在某届世界杯中的进球数
DROP procedure IF EXISTS get_confHomeGoals;
DELIMITER $$
CREATE DEFINER=root@localhost PROCEDURE get_confHomeGoals(
IN _year year
)
BEGIN
select confederation, count(*)
from 
    (SELECT M.home AS ccode
    FROM Goal G, Matches M
    WHERE G.hscore = 1 AND G.mid = M.mid AND M.cyear = _year
    ) AS MM, NationalTeam N
    where MM.ccode = N.ccode
    group by confederation;
END$$
DELIMITER ;

DROP procedure IF EXISTS get_confAwayGoals;
DELIMITER $$
CREATE DEFINER=root@localhost PROCEDURE get_confAwayGoals(
IN _year year
)
BEGIN
select confederation, count(*)
from 
    (SELECT M.away AS ccode
    FROM Goal G, Matches M
    WHERE G.hscore = 0 AND G.mid = M.mid AND M.cyear = _year
    ) AS MM, NationalTeam N
    where MM.ccode = N.ccode
    group by confederation;
END$$
DELIMITER ;


DROP procedure IF EXISTS get_MaxLeagueContr;
DELIMITER $$
CREATE DEFINER=root@localhost PROCEDURE get_MaxLeagueContr(
IN _year year
)
BEGIN
CREATE TEMPORARY TABLE IF NOT EXISTS leaguecontr AS
(
    SELECT C.league, COUNT(*) AS nplayer
    FROM Club C, Player P, playsForCup PFC, ntplaysin NTP
    WHERE C.clname = P.clname AND PFC.pid = P.pid AND PFC.ncid = NTP.ncid and ntp.cyear = _year
    GROUP BY C.league, NTP.cyear
);
select league,nplayer from LeagueContr
where nplayer = (
    select max(nplayer) from leaguecontr
);
END$$
DELIMITER ;

