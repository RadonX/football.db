- requirement
 Flask
 [Flask-MySQL — flask-mysql 1.3 documentation](https://flask-mysql.readthedocs.org/en/latest/#)
 [Flask-Bootstrap — Flask-Bootstrap 3.3.5.7 documentation](http://pythonhosted.org/Flask-Bootstrap/index.html)

- run
 1. build the necessary database (`football.sql`) and create functions and procedures (`proc.sql`).
 2. ```> python app.py ```
 3. go to `localhost:5000`