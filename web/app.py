from flask import *
#from flask import Flask, render_template, json, request,redirect,session,jsonify
from flask.ext.bootstrap import Bootstrap
from flask.ext.mysql import MySQL
# https://flask-mysql.readthedocs.org/en/latest/#


app = Flask(__name__)
Bootstrap(app)

mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'soccer2'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

def find_max(data,ind):
	#print data
	print data
	maxitem = data[0]
	for item in data:
		if item[ind]>maxitem[ind]:
			maxitem = item
	return maxitem

@app.route("/")
def index():
	result = {}
	try:
		con = mysql.connect()
		cursor = con.cursor()

		cursor.callproc('get_LeagueContr',('2014',))
		data = cursor.fetchall()
		result['maxleague'] = find_max(data,1)
		cursor.close()
		cursor = con.cursor()

		data3 = {}
		cursor.callproc('get_confHomeGoals',('2014',))
		confHomeGoals = cursor.fetchall()
		cursor.close()
		cursor = con.cursor()
		cursor.callproc('get_confAwayGoals',('2014',))
		confAwayGoals = cursor.fetchall()
		result['confAwayGoals'] = confAwayGoals
		result['confHomeGoals'] = confHomeGoals
		for item in confHomeGoals:
			data3[item[0]] = item[1]
		for item in confAwayGoals:
			data3[item[0]] += item[1]
		data = []
		for k in data3:
			data.append((k,data3[k]) )
		result['maxconfederation'] = find_max(data,1)

		cursor.close()
		cursor = con.cursor()
		sql0 = 'SELECT round, mdate, home, away, mscore, attendance'\
				' FROM matches WHERE Year(mdate) = 2014'\
				' ORDER BY attendance DESC LIMIT 10'
		cursor.execute(sql0)
		result['matches'] = cursor.fetchall()
		cursor.close()


		return render_template('index.html',res = result,scorer=None)
	except Exception as e:
		return render_template('error.html', error = str(e))


@app.route('/get_NationalTeam')
def get_NationalTeam():
    try:
        con = mysql.connect()
        cursor = con.cursor()
        cursor.callproc('get_NationalTeam',('UEFA',))
        data = cursor.fetchall()

        teams_dict = []
        for team in data:
            team_dict = {
                    'id':team[0], #ccode
                    'name':team[1] #country
                    }
            teams_dict.append(team_dict)
        return json.dumps(teams_dict)

    except Exception as e:
        return render_template('error.html', error = str(e))



@app.route("/matches")
def matches():
	matches_data = getMatchesData()
	return render_template('matches.html', data = matches_data)

@app.route("/players/")
def players():
	players = getPlayers()	
	result = {}
	result["ccode"] = getNationalTeam(2014)
	return render_template('players.html',players = players, res = result)	

@app.route("/players/<ccode>")
def players_ccode(ccode):
	'''
	if ccode == "":
		players = getPlayers()	
		result = {}
		result["ccode"] = getNationalTeam(2014)
		return render_template('players.html',players = players, res = result)	
	'''
	try:
		conn = mysql.connect()
		cursor = conn.cursor()
		query = "SELECT pname, pnationality, pdob, pposition, clname '\
				'FROM player where pnationality = '{}'".format(ccode)
		cursor.execute(query)
		players = cursor.fetchall()
		result = {}
		result["ccode"] = getNationalTeam(2014)
		return render_template('players.html',players = players, res = result)	
	except Exception as e:
		return render_template('error.html',error = str(e))
	finally:
		cursor.close()
		con.close()



@app.route("/player/<player>")
def player():
	player = getPlayerData(player)
	return render_template('players.html',player = player)

@app.route("/nationalteams")
def nationalteams():
	year_2006 = getNationalTeam(2006)
	year_2010 = getNationalTeam(2010)
	year_2014 = getNationalTeam(2014)
	return render_template('nationalteams.html', year_2014 = year_2014, year_2010 = year_2010, year_2006 = year_2006)


@app.route("/getteambyname/<team_name>")
def getteambyname(team_name):
	data = getTeamData(team_name)
	return render_template('team.html', name=team_name, country = data[0][0], confederation = data[0][1], pname = data[0][2] )



@app.route('/validateLogin',methods=['POST'])
def validateLogin():
	try:
		ccode = request.form['ccodesel']
		return redirect('/players/{}'.format(ccode))
	except Exception as e:
		return render_template('error.html',error = str(e))
	finally:
		cursor.close()
		con.close()


def getMatchesData():
	conn = mysql.connect()
	cursor = conn.cursor()
	query = "SELECT mdate, home, away, mscore, attendance FROM Matches LIMIT 100"
	cursor.execute(query)
	data = cursor.fetchall()
	cursor.close()
	conn.close()
	return data

def getTeamData(team_name):
	conn = mysql.connect()
	cursor = conn.cursor()
	cursor.callproc('get_countryInfo',(team_name,))
	data = cursor.fetchall()	
	cursor.close()
	conn.close() 	
	return data

def getNationalTeam(year):
	conn = mysql.connect()
	cursor = conn.cursor()
	sql1 = 'SELECT ccode FROM ntplaysin WHERE cyear = {}'.format(str(year))
	cursor.execute(sql1)
	data = cursor.fetchall()
	cursor.close()
	conn.close()
	return data

def getPlayers():
	conn = mysql.connect()
	cursor = conn.cursor()
	query = "SELECT pname, pnationality, pdob, pposition, clname FROM player LIMIT 1000"
	cursor.execute(query)
	data = cursor.fetchall()
	cursor.close()
	conn.close()
	return data

def  getPlayerData():
	sql0 = "select * FROM playsForCup where ncid = ncID('{}','WC','2014')".format(ccode)


if __name__ == "__main__":
    app.run(debug=True)