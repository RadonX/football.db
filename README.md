# football.db

- /web
 a very naive web application of this football database. 

- database
 
 - [ER diagram](https://drive.google.com/a/columbia.edu/file/d/0B5yiAo58sISqSUxudjVvZ1JBWHc/view?usp=sharing_eid&ts=56094957)
   open in <draw.io>
 
 - build the necessary database (`football.sql`) and create functions and procedures (`proc.sql`).
   mysql -u root -p[root_password] [database_name] < filename.sql
