#[Tables - football.db Schema Docu](http://openfootball.github.io/schema/)


DROP DATABASE IF EXISTS football; ##
CREATE DATABASE football;
USE football;

#-- http://stackoverflow.com/questions/13543211/sql-constraint-check
#-- on delete
# DEFAULT CHARSET=utf8;


# ID: ALTER TABLE tbl_name ENGINE = MyISAM OR BDB;
    # SELECT ENGINE FROM INFORMATION_SCHEMA.TABLES
    # WHERE TABLE_SCHEMA = 'cookbook' AND TABLE_NAME = 'insect';
# SET @saved_id =  LAST_INSERT_ID(); # cookbook P496


CREATE TABLE Club (
    clid INT UNSIGNED AUTO_INCREMENT,
    clname VARCHAR(40) NOT NULL,
    league VARCHAR(30) NOT NULL,#use country for the time being!!!!
    # and assume
    PRIMARY KEY (clid)
);

#--- 


CREATE TABLE NationalTeam(
    ccode CHAR(4), 
    confederation VARCHAR(10),
    continent VARCHAR(15),
    ranking INT UNSIGNED , #update. historical ranking?
    cchid INT UNSIGNED UNIQUE,# NOT NULL, isCoaching
    #FOREIGN KEY (cchid) REFERENCES Coach(cchid), #ADD WITH ALTER
    PRIMARY KEY(ccode)
);

CREATE TABLE CountryName (
    ccode CHAR(4), 
    country VARCHAR(40),
    #FOREIGN KEY (ccode) REFERENCES NationalTeam(ccode), 
    #b/c it's inconvenient to always add NationalTeam before CountryName
    PRIMARY KEY (country)
); #for the same country with mutiple names 

#--- 

CREATE TABLE Coach(
    cchid INT UNSIGNED AUTO_INCREMENT,
    cchname VARCHAR(30) NOT NULL,
    cchnationality CHAR(4) NOT NULL,
    cchdob DATE,
    PRIMARY KEY(cchid),
    FOREIGN KEY (cchnationality) REFERENCES NationalTeam(ccode)
);

ALTER TABLE NationalTeam 
ADD FOREIGN KEY (cchid)
REFERENCES Coach(cchid);

CREATE TABLE Player(
    pid INT UNSIGNED AUTO_INCREMENT,
    #pfirstname VARCHAR(30), 
    #plastname VARCHAR(30),
    #ESPN data is good in name, but need a scraper for other info
    pname VARCHAR(60) NOT NULL, # `display name`, UNIQUE actually
    pnationality CHAR(4), 
    pdob DATE,
    pposition VARCHAR(10) NOT NULL CHECK (pposition IN ('FW','MF','DF','GK')),
    clname VARCHAR(40), # belongsToCL, clid!!! will need clname->clid
    #UNIQUE (pname,pdob)
    FOREIGN KEY (pnationality) REFERENCES NationalTeam(ccode),
    FOREIGN KEY (clname) REFERENCES Club(clname),
    PRIMARY KEY(pid)
);

CREATE TABLE PlayerAlias (
    pid INT UNSIGNED, # import ESPN id
    pnames VARCHAR(60),
    FOREIGN KEY (pid) REFERENCES Player(pid)
); #for the same country with mutiple names 


CREATE TABLE Competition(
    ctype VARCHAR(16),
    cyear YEAR(4),
    #chonor VARCHAR(16),
    #place
    PRIMARY KEY(ctype,cyear)
);


CREATE TABLE ntPlaysIn (
    ncid INT UNSIGNED AUTO_INCREMENT,
    ccode CHAR(4) NOT NULL,
    ctype VARCHAR(16) NOT NULL,
    cyear YEAR(4), # NOT NULL,
    #nstanding TINYINT UNSIGNED, 
    FOREIGN KEY (ccode) REFERENCES NationalTeam (ccode),
    FOREIGN KEY (ctype,cyear) REFERENCES Competition (ctype,cyear),
    UNIQUE (ccode,ctype,cyear),
    PRIMARY KEY (ncid)
);

CREATE TABLE playsForCup (
    pid INT UNSIGNED,
    ncid INT UNSIGNED,
    pnum TINYINT UNSIGNED, 
    iscap BOOL, #attr?
    pcap SMALLINT, 
    FOREIGN KEY (ncid) REFERENCES ntPlaysIn (ncid),
    FOREIGN KEY (pid) REFERENCES Player (pid),
    PRIMARY KEY (pid, ncid)
);



CREATE TABLE Matches(
    mid INT UNSIGNED AUTO_INCREMENT, # my data contains mid
    mdate DATE NOT NULL,
    mscore VARCHAR(16), -- 2-0 (5-4)
    ctype VARCHAR(16) NOT NULL,
    cyear YEAR(4) NOT NULL, #used to indentify Competiiton, redundancy to mdate,
    attendance , 
    home CHAR(4), # ccode info included in play, connected with NationalTeam
    away CHAR(4),
    FOREIGN KEY (home) REFERENCES NationalTeam (ccode),
    FOREIGN KEY (away) REFERENCES NationalTeam (ccode),
    FOREIGN KEY (ctype, cyear) REFERENCES Competition (ctype, cyear),
    PRIMARY KEY(mid), #or (mdate, home, away)
    CHECK (REGEXP_LIKE(mscore, '[0-9]+-[0-9]+ (\([0-9]+-[0-9]+\))?'))
);


CREATE TABLE Goals(
    gid INT UNSIGNED,
    gtime VARCHAR(16), -- 90+1 
    mid INT UNSIGNED, #scoredin
    PRIMARY KEY(gid),
    FOREIGN KEY (mid) REFERENCES Matches (mid),
    #CHECK(REGEXP_LIKE(gtime, '[0-9][0-9]?(\+[0-9])?') )
);


CREATE TABLE Goal(
    gid INT UNSIGNED, # AUTO_INCREMENT,
    mid INT UNSIGNED NOT NULL, #scoredin
    pid INT UNSIGNED,
    gminute TINYINT UNSIGNED,
    gtype VARCHAR(6), #pen, own
    status VARCHAR(6), #3-2
    gadded TINYINT UNSIGNED, #45+, 90+
    FOREIGN KEY (mid) REFERENCES Matches (mid),
    FOREIGN KEY (pid) REFERENCES Player (pid),
    #CHECK(REGEXP_LIKE(gtime, '[0-9][0-9]?(\+[0-9])?') ),
    PRIMARY KEY(mid,gid)
);


CREATE TABLE Foul(
    fid INT UNSIGNED,
    ftype CHAR(4),
    mid INT UNSIGNED, #conductedin
    pid INT UNSIGNED,    
    PRIMARY KEY(fid),
    FOREIGN KEY (pid) REFERENCES Player (pid),
    FOREIGN KEY (mid) REFERENCES Matches (mid),
    CHECK(ftype in ('Y','R')) -- yellow/red cards
);


CREATE TABLE coached(
    cchid INT UNSIGNED,
    ccode CHAR(4),
    startTime DATE,
    endTime DATE,
    PRIMARY KEY (cchid, ccode),
    FOREIGN KEY (cchid) REFERENCES Coach (cchid),
    FOREIGN KEY (ccode) REFERENCES NationalTeam (ccode),
    CHECK (startTime < endTime)
);

CREATE TABLE Forward(
    fwdid INT UNSIGNED AUTO_INCREMENT,
    numfwd INTEGER,
    ccode CHAR(4),
    FOREIGN KEY (ccode) REFERENCES NationalTeam (ccode) ON DELETE CASCADE,
    PRIMARY KEY(fwdid)
);

CREATE TABLE Midfield(
    midid INT UNSIGNED AUTO_INCREMENT,
    nummid INTEGER,
    ccode CHAR(4),
    FOREIGN KEY (ccode) REFERENCES NationalTeam (ccode) ON DELETE CASCADE,
    PRIMARY KEY(midid)
);

CREATE TABLE Defender(
    defid INT UNSIGNED AUTO_INCREMENT,
    numdef INTEGER,
    ccode CHAR(4),
    FOREIGN KEY (ccode) REFERENCES NationalTeam (ccode) ON DELETE CASCADE,
    PRIMARY KEY(defid)
);

CREATE TABLE fwdhas (
    fwdid INT UNSIGNED,
    pid INT UNSIGNED,
    #pnum INTEGER, # jersey number
    #fwdiid INTEGER, #NULL,2,3,..., indicate squad with different pnum
    PRIMARY KEY (fwdid, pid),
    FOREIGN KEY (fwdid) REFERENCES Forward (fwdid),
    FOREIGN KEY (pid) REFERENCES Player (pid)
);
CREATE TABLE midhas (
    midid INT UNSIGNED,
    pid INT UNSIGNED,
    PRIMARY KEY (midid, pid),
    FOREIGN KEY (midid) REFERENCES Midfield (midid),
    FOREIGN KEY (pid) REFERENCES Player (pid)
);
CREATE TABLE defhas (
    defid INT UNSIGNED,
    pid INT UNSIGNED,
    PRIMARY KEY (defid, pid),
    FOREIGN KEY (defid) REFERENCES Defender (defid),
    FOREIGN KEY (pid) REFERENCES Player (pid)
);


CREATE TABLE squad (
    mid INT UNSIGNED,
    ccode CHAR(4),
    lineup VARCHAR(16),
    fwdid INT UNSIGNED,
    midid INT UNSIGNED,
    defid INT UNSIGNED,
    FOREIGN KEY (mid) REFERENCES Matches (mid),
    FOREIGN KEY (fwdid) REFERENCES Forward (fwdid),
    FOREIGN KEY (midid) REFERENCES Midfield (midid),
    FOREIGN KEY (defid) REFERENCES Defender (defid),
    CHECK(REGEXP_LIKE(lineup, '[0-9]-[0-9]-([0-9]-)?1') ),
    PRIMARY KEY (mid,ccode)
);

CREATE TABLE participated ( # not using this b/c I don't have so much info
    pid INT UNSIGNED, 
    gid INT UNSIGNED,
    ptype VARCHAR(16),
    CHECK (ptype in ('assist', 'score', 'owngoal')), #abbr.
    PRIMARY KEY (pid, gid),
    FOREIGN KEY (pid) REFERENCES Player (pid),
    FOREIGN KEY (gid) REFERENCES Goals (gid) ON DELETE CASCADE
);


# -----------------

#CREATE TABLE belongsToNT (
