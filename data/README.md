#### GitHub

- [sanand0/fifadata](https://github.com/sanand0/fifadata)

 > scraper to pull every world cup's result from linguasport.com, fifa.com and Wikipedia. 
 > ref: [pratapvardhan/FIFAWorldCup](https://github.com/pratapvardhan/FIFAWorldCup)
 
 - `fifa-wc-squads.py`->`fifa-wc-squads.csv`: http://en.wikipedia.org/wiki/"+str(year)+"_FIFA_World_Cup_squads. but there are some bugs to fix before getting a perfect format.

 - [linguasport.com](http://www.linguasport.com/futbol/internacional/mundial/seekff.asp)
  [scraper](http://nbviewer.ipython.org/github/sanand0/fifadata/blob/master/matches.ipynb)
  has almost all the info of every match

- [jokecamp/FootballData](https://github.com/jokecamp/FootballData)

 A hodgepodge of JSON and CSV Football/Soccer data 


#### National Teams

- [`openmundi/world.csv`](https://github.com/openmundi/world.csv)

 > ref: [FIFA country codes - Wikipedia](https://en.wikipedia.org/wiki/List_of_FIFA_country_codes)
  historical: a country has ceased to exist
  obsolete: a country has changed its code,

- [ccodes.txt](http://www.rsssf.com/tablesc/ccodes.txt)

- [`SoccerStatsUS/soccerdata/confederations`](https://github.com/SoccerStatsUS/soccerdata/blob/master/data/confederations)


#### Other data source

0. Catalog

 - [planetopendata/awesome-football](https://github.com/planetopendata/awesome-football)

 - [World Cup 2014 | The Huffington Post](http://data.huffingtonpost.com/2014/world-cup)


1. RSSSF

 - [World Cup Overview](http://www.rsssf.com/tablesw/worldcup.html)
  [The World Cup Archive](http://www.rsssf.com/tablesw/wcf-full-intro.html)
  [World Cup All-Time Tables (including Qualifying)](http://www.rsssf.com/tablesw/wcq-records.html)
  [World Cup Final Tournaments 1930-2014 - Total Rankings](http://www.rsssf.com/tables/3002f.html)
  [World Cup 1930-2014 - Info on Coaches](http://www.rsssf.com/players/3098f-coaches.html)

 - [The RSSSF Archive - Current International Tournaments](http://www.rsssf.com/curtour.html)

 > +all scores of all qualifying
 
 
2. [soccerworldcups.com](http://thesoccerworldcups.com/world_cups.php) has the results, playoffs, top scorers and final standings across world cups

3. [Index of /national_teams_kits](http://www.colours-of-football.com/national_teams_kits/)

4. [Squads - National Competitions](http://www.footballsquads.co.uk/national.htm)

10. [All-time American soccer statistics | SoccerStats.us](http://soccerstats.us/)
