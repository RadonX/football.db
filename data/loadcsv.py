#!/usr/bin/python
# -*- coding: utf-8 -*-

from DBinsertor import DBinsertor
import re
import csv

# [python-pandas and databases like mysql](http://stackoverflow.com/questions/10065051/python-pandas-and-databases-like-mysql)

def get_ccode(db,str_):
    db.cursor.execute('SELECT countryCode("{}")'.format(str_))
    ccode = db.cursor.fetchone()
    ccode = ccode[0]
    if ccode is None:
        db.cursor.execute('SELECT 1 FROM NationalTeam WHERE ccode = "{}"'.format(str_))
        ccode = db.cursor.fetchone()
        if ccode[0]: # if str_ is ccode
            return str_
        print '-- Country',str_,'not found --'
    return ccode

def get_pid(db,str_):
    db.cursor.execute('SELECT playerID("{}")'.format(str_))
    pid = db.cursor.fetchone()
    pid = pid[0]
    if pid is None:
        pid = 'Null'
        print '-- Player',str_,'not found --'
    return pid


def ins_NationalTeam(db):
    reg = re.compile('\s*(\w+),(.+?)(\s{2,}|\n)') # CONMEBOL, Argentina
    path = 'confederations.txt'
    sql0 = "REPLACE INTO NationalTeam(ccode, confederation) VALUES (countryCode('{}'), '{}')"

    inputfile = open(path)
    while True:
        line = inputfile.readline()
        if line == "": break
        if line.find('*') == -1: #should insert this country! check confederation.txt
            result = reg.match(line)
            if result:
                try:
                    country = result.group(2)
                    if country[0] == ' ': country = country[1:]
                    if country[-1] == ' ': country = country[:-1]
                    db.cursor.execute(sql0.format(country,result.group(1)))
                    data = db.cursor.fetchall()
                except Exception as e:
                    print country,result.group(1)
                    print e
                    #print sql0.format(country,result.group(1))

    db.db.commit()
    inputfile.close()


def ins_Player(db, redate):
    path = 'fifa-wc-squads.csv'
    # No,Pos,Player,DOB/Age,Caps,Club,Country,ClubCountry,Year
    sql0 = 'INSERT INTO Player (pname,pnationality,pdob,pposition,clname) ' \
          'SELECT * FROM (SELECT {}) AS tmp ' \
          'WHERE (playerID("{}") IS NULL )'
    # no update for the same player.

    def deletesp(str_): #delete ', ' in the source file directly is better
        if str_[-1] == ' ':
            str_ = str_[:-1]
        s = 0
        if str_[0] == ' ':
            s = 1
        return str_[s:]
    def formatname(pname):
        end = pname.find(' (c)')
        if end != -1:
            pname = pname[0:end]
        return pname.replace('"','\\"')

    inputfile = open(path)
    reader = csv.reader(inputfile)
    for row in reader:

        date = redate.search(row[3])
        if date:
            date = date.group()
        else:
            date = 'NULL'
            #print row[2], row[3]
        pos = re.compile(r'(?:[1-4])(GK|DF|MF|FW)').match(row[1])
        if pos:
            pos = pos.group(1)
        else:
            pos = 'NULL'
            #print '@',row[2], row[1]
        pname = formatname(row[2])

        print pname
        try:
            values = '"{}",countryCode("{}"),"{}","{}","{}" '.format(pname,deletesp(row[6]),date,pos,row[5])
            sql = sql0.format(values,pname)
            db.cursor.execute(sql)
            data = db.cursor.fetchall()
        except Exception as e:
            db.db.commit()
            print row
            print sql
            print e
            raise

    db.db.commit()
    inputfile.close()


def ins_Club(db):
    path = 'fifa-wc-squads.csv'
    # No,Pos,Player,DOB/Age,Caps,Club,Country,ClubCountry,Year
    sql0 = 'INSERT INTO Club (clname, league) ' \
          'SELECT * FROM (SELECT "{}","{}") AS tmp ' \
          'WHERE NOT EXISTS (SELECT clid FROM Club WHERE clname = "{}")'
    # no using ON DUPLICATE b/c clname is not key

    def checksp(str_):
        if str_[-1] == ' ' or str_[0] == ' ':
            print(str_)

    inputfile = open(path)
    reader = csv.reader(inputfile)
    count = 0
    for row in reader:
        try:
            sql = sql0.format(row[5],row[7],row[5])
            db.cursor.execute(sql)
            data = db.cursor.fetchall()
            count += 1
        except Exception as e:
            db.db.commit()
            print row
            print sql
            print e
            raise

    db.db.commit()
    print count
    inputfile.close()


def ins_pnc(db):
    path = 'fifa-wc-squads.csv'
    # No,Pos,Player,DOB/Age,Caps,Club,Country,ClubCountry,Year
    sql1 = 'INSERT IGNORE INTO ntPlaysIn (ccode,ctype,cyear) VALUES ("{}","WC",{})'
    sql2 = 'INSERT IGNORE INTO playsForCup (pid,ncid,pnum,iscap,pcap) ' \
          'VALUES (playerID("{}"),{},{},{},{})'
    cap = ['False','True']

    def checksp(str_):
        if str_[-1] == ' ' or str_[0] == ' ':
            print(str_)
    def formatname(pname):
        end = pname.find(' (c)')
        if end != -1:
            pname = pname[0:end]
            iscap = 1
        else: iscap = 0
        return pname.replace('"','\\"'), iscap

    inputfile = open(path)
    reader = csv.reader(inputfile)
    for row in reader:
        checksp(row[6])
        try:
            # get ccode(country)
            db.cursor.execute('SELECT countryCode("{}")'.format(row[6]))
            ccode = db.cursor.fetchone()
            ccode = ccode[0]
            if ccode is None:
                print row,'not found'
                continue
            # get ncID
            db.cursor.execute('SELECT ncID("{}","WC",{})'.format(ccode, row[8]) )
            ncid = db.cursor.fetchone()
            if ncid[0] is None:
                # insert ntplaysin
                db.cursor.execute(sql1.format(ccode,row[8] ))
                db.cursor.execute('SELECT LAST_INSERT_ID()')
                ncid = db.cursor.fetchone()
            # insert playsForCup
            pname, iscap = formatname(row[2])
            if iscap:
                print pname
            #db.cursor.execute('UPDATE Player SET pnationality = "{}" WHERE pname = "{}"'.format(ccode,pname))
            pnum = 'NULL'
            if row[0].isdigit() and int(row[0])>0:
                pnum = row[0]
            if row[4]:
                pcap = row[4]
            else: pcap = 'NULL'
            sql = sql2.format(pname,str(ncid[0]),pnum,cap[iscap],pcap )
            db.cursor.execute(sql)
        except Exception as e:
            db.db.commit()
            print row
            print sql
            print e
            raise

    db.db.commit()
    inputfile.close()


def ins_Match(db):
    path = 'games.csv'
    # ATTENDANCE,DATE,PK,REFEREE,STADIUM,game_id,stage,team1,team2,url
    sql0 = 'INSERT IGNORE INTO Matches (attendance, mdate, mid, round, home, away,ctype,cyear) ' \
           'VALUES ({},"{}",{},"{}","{}","{}","WC","{}")'
    redate = re.compile('([0-9]{1,2})-([0-9]{2})-([0-9]{4})' ) #1-07-2006

    inputfile = open(path)
    inputfile.readline() #ignore first row
    reader = csv.reader(inputfile)

    count = 0
    for row in reader:
        try:
            home = get_ccode(db,row[7])
            away = get_ccode(db,row[8])
            if (home is None) or (away is None):
                continue
            date = redate.match(row[1])
            year = date.group(3)
            date = "{}-{}-{}".format(year,date.group(2),date.group(1))
            sql = sql0.format(row[0],date,row[5],row[6],home,away,year)
            db.cursor.execute(sql)
            count += 1
        except Exception as e:
            db.db.commit()
            print row
            print sql
            print e
            raise

    db.db.commit()
    inputfile.close()
    print count


def update_mscore(db,mid,mscore):
    sql0 = 'UPDATE Matches SET mscore = "{}" WHERE mid = {}'
    try:
        sql = sql0.format(mscore,mid)
        db.cursor.execute(sql)
        db.db.commit()
    except:
        print sql
        print e

def ins_Goal(db):
    path = 'goals.csv'
    # game_id,minute,player,team1score,team2score
    sql0 = 'REPLACE INTO Goal (mid, gid,pid, gminute, gtype, status, gadded,hscore) ' \
           'VALUES ({},{},{},{},{},"{}",{},{})'
           #'SELECT {} FROM Goal WHERE NOT EXISTS  ' \

    def formatname(pname):
        gtype = 'Null'
        end = pname.find(' [p.]')
        if end > 0:
            gtype = '"pen"'
            pname = pname[0:end]
        else:
            end = pname.find(' [o.g.]')
            if end > 0:
                gtype = '"own"'
                pname = pname[0:end]
        return pname, gtype #.replace('"','\\"')

    def isadded(minute):
        gadded = 'Null'
        if minute[-1] == '+':
            gadded = '"{}"'.format(minute) #TINYINT, 90+ -> 90
            minute = minute[:-1]
        return minute,gadded

    inputfile = open(path)
    inputfile.readline() #ignore first row
    reader = csv.reader(inputfile)

    count = 0
    mid = '0'
    status = '0-0'
    for row in reader:
        try:
            # start new match
            if row[0] != mid:
                update_mscore(db,mid,status)
                mid = row[0]
                gid = 0
                homescore = 0
            gid += 1
            if int(row[3]) == homescore:
                hscore = '0'
            else:
                hscore = '1'
                homescore += 1
            pname,gtype = formatname(row[2])
            minute,gadded = isadded(row[1])
            status = '{}-{}'.format(row[3],row[4]) #2-3
            sql = sql0.format(mid,str(gid),get_pid(db,pname),minute,gtype,status,gadded,hscore)
            db.cursor.execute(sql)
            count += 1
        except Exception as e:
            db.db.commit()
            print row
            print sql
            print e
            raise


    db.db.commit()
    inputfile.close()
    print count

if __name__ == "__main__":
    pattern = r'\b(?:(?:(?:(?:[1-9][0-9])(?:0[48]|[2468][048]|[13579][26])|' \
              r'(?:(?:[2468][048]|[13579][26])00))(\/|-|\.)(?:0?2\1(?:29)))|' \
              r'(?:(?:[1-9][0-9]{3})(\/|-|\.)(?:(?:(?:0?[13578]|1[02])\2(?:31))|' \
              r'(?:(?:0?[13-9]|1[0-2])\2(?:29|30))|' \
              r'(?:(?:0?[1-9])|(?:1[0-2]))\2(?:0?[1-9]|1\d|2[0-8]))))\b'
    redate = re.compile(pattern)
    dbInsertor = DBinsertor()
    #ins_NationalTeam(dbInsertor)
    #ins_Player(dbInsertor, redate)
    #ins_Club(dbInsertor)
    #ins_pnc(dbInsertor)
    #ins_Match(dbInsertor)
    ins_Goal(dbInsertor)
    dbInsertor.close()


