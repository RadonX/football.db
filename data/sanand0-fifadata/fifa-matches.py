
import os
import re
import hashlib
import requests
import pandas as pd
from lxml.html import parse

if not os.path.exists('.cache'):
    os.makedirs('.cache')

ua = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36'
session = requests.Session()

def get(url):
    '''Return cached lxml tree for url'''
    path = os.path.join('.cache', hashlib.md5(url).hexdigest() + '.html')
    if not os.path.exists(path):
        print url
        response = session.get(url, headers={'User-Agent': ua})
        with open(path, 'w') as fd:
            fd.write(response.text.encode('utf-8'))
    return parse(open(path))

editions = [
    'southafrica2010',
    'germany2006',
    'koreajapan2002',
    'france1998',
    'usa1994',
    'italy1990',
    'mexico1986',
    'spain1982',
    'argentina1978',
    'germany1974',
    'mexico1970',
    'england1966',
    'chile1962',
    'sweden1958',
    'switzerland1954',
    'brazil1950',
    'france1938',
    'italy1934',
    'uruguay1930',
]

match_re = re.compile(r'/round=(\d+)/match=(\d+)/')
team_re = re.compile(r'/team=(\d+)/')

stage_replace = {
  'Group 1': 'Group A',
  'Group 2': 'Group B',
  'Group 3': 'Group C',
  'Group 4': 'Group D',
  'Group 5': 'Group E',
  'Group 6': 'Group F',
  'First round': 'Group A',
  'Preliminary round': 'Group A',
  'Match for third place': 'Third place',
}

matches, team_name = [], {}
for edition in editions:
    url = 'http://www.fifa.com/tournaments/archive/worldcup/{:s}/matches/index.html'
    tree = get(url.format(edition))
    for table in tree.findall('.//table'):
        caption = table.find('caption')
        if caption is None:
            continue
        stage = caption.text
        for tr in table.findall('.//tbody/tr'):
            cells = tr.findall('td')
            result = cells[4].find('.//a').text
            match_id = match_re.search(cells[5].find('.//a').get('href'))
            flag1 = cells[3].find('.//span').get('class')[-3:].upper()
            flag2 = cells[7].find('.//span').get('class')[-3:].upper()
            team_id1 = team_re.search(cells[4].find('a').get('href'))
            team_id2 = team_re.search(cells[6].find('a').get('href'))
            team_name[flag1] = cells[4].find('a').text
            team_name[flag2] = cells[6].find('a').text
            
            row = {
                'Edition': edition,
                'Venue': cells[2].text.strip(),
                'Match': cells[0].text,
                'Date': cells[1].text + ' ' + edition[-4:],
                'Stage': stage_replace.get(stage, stage),
                'Country1': flag1,
                'Country2': flag2,
                'Result': cells[5].find('.//a').text,
                'TeamID1': team_id1.group(1),
                'TeamID2': team_id2.group(1),
                'RoundID': match_id.group(1),
                'MatchID': match_id.group(2),
            }
            matches.append(row)

re_player = re.compile(r'/worldfootball/statisticsandrecords/players/player=(\d+)/index.html')

teams = []
for edition in editions:
    url = 'http://www.fifa.com/tournaments/archive/worldcup/{:s}/teams/index.html'
    tree = get(url.format(edition))
    team_cols = ['No', 'Name', 'DOB', 'Position', 'Height']
    for team in tree.xpath('.//div[contains(@class, "tTeamsClean")]//a'):
        flag = team.find('.//span')
        if flag is not None:
            flag = flag.get('class')[-3:].upper()
            url = team.get('href')
            if edition == 'southafrica2010':
                url = url.replace('index.html', 'squadlist.html')
            subtree = get('http://www.fifa.com' + url)
            for row in subtree.findall('.//tbody//tr'):
                vals = [cell.text_content().strip() or '' for cell in row.findall('td')]
                player = dict(zip(team_cols, vals))
                player['Edition'] = edition
                player['Country'] = flag
                link = row.xpath('.//a[contains(@class, "teamstats-player")]')
                if len(link):
                    url = link[0].get('href', 'x')
                    match = re_player.match(url)
                    if match:
                        player['ID'] = match.group(1)
                teams.append(player)

            coach = subtree.xpath('.//div[@class="tGroupDetail"]/div')
            coach = coach[0].text_content().strip()
            teams.append({
                'Edition': edition,
                'Country': flag,
                'Position': 'Coach',
                'Name': re.sub(r'\s+', ' ', coach)
            })

teams = pd.DataFrame(teams)

event_re = re.compile(r' *,? *(.*?) \(([A-Z]+)\) (\d+\'|PSO)')
pso_re = re.compile(r'(.*?) \d+')
players = teams.set_index(['Edition', 'Country', 'Name'])['ID']

rename_player = {
    'HONG Myungbo': 'HONG Myung Bo', # KOR, usa1994
    'Ricardo PELAEZ': 'Ricardo PELAEZ LINARES', # MEX, france1998
    'HWANG Bokwan': 'HWANG Bo Kwan', # KOR, italy1990
    'HUH Jungmoo': 'HUH Jung Moo', # KOR, mexico1986
    'JOSE TORRES': 'Jose TORRES', # POR, england1966
    'ANTONIO SIMOES': 'Antonio SIMOES', # POR, england1966
    'MARIO COLUNA': 'Mario COLUNA', # POR, england1966
    'Nikita SIMONYAN': 'Nikita SIMONIAN', # URS, sweden1958
    'KIM Taeyoung': 'KIM Tae Young', # KOR, koreajapan2002, france1998
    # - Dobromir JECHEV is sometimes spelt at Dobromir ZECHEV of BUL? Has he played 4 cups?
}

stats = {
 'Fouls Committed': 'Fouls',
 'Cautions': 'Cautions',
 'Direct Expulsions': 'Expulsions',
 'Expulsions due to Second Caution': 'Second Cautions',
 'Corner kicks': 'Corner Kicks',
 'Direct Free Kicks to Goal': 'Direct Free Kicks',
 'Indirect Free Kicks to Goal': 'Indirect Free Kicks',
 'Penalty Kicks': 'Penalty Kicks',
 'Offsides': 'Offsides',
 'Own Goals': 'Own',
 'Possession (%)': 'Possession',
 'Shots': 'Shots',
 'Shots on goal': 'On Goal'
}

def player_id(match, name):
    name = rename_player.get(name, name)
    id = players.get((match['Edition'], match['Country1'], name))
    if id is None:
        id = players.get((match['Edition'], match['Country2'], name))
    if id is not None:
        # Tunisia has duplicate players
        # http://www.fifa.com/tournaments/archive/worldcup/koreajapan2002/teams/team=43888/index.html
        if type(id) is not str:
            id = id[-1]
        return id
    else:
        print 'Missing', match['Edition'], match['Country1'], match['Country2'], name, hashlib.md5(url).hexdigest() + '.html'
        return None, None

events  = []
for match in matches:
    url = 'http://www.fifa.com/tournaments/archive/worldcup/{Edition:s}/matches/round={RoundID:s}/match={MatchID:s}/index.html'.format(**match)
    if match['Edition'] == 'southafrica2010':
        url = url.replace('index.html', 'report.html')
    tree = get(url)

    # Summary
    vals = tree.xpath('.//div[@class="hdTeams"]//tbody//td')
    match['Time'] = vals[2].text
    match['Attendance'] = vals[-1].text

    # Stats
    stats_table = tree.find('.//table[@class="summaryStatistics"]')
    if stats_table is not None:
        for row in stats_table.findall('.//tbody/tr'):
            val1, stat, val2 = [cell.text for cell in row.findall('td')]
            if stat in stats:
                stat = stats[stat]
                match[stat + '1'], match[stat + '2'] = val1, val2
    
    lists = tree.xpath('.//div[contains(@class, "mrep")]//ul')
    # G = Goal, C = Caution, E = Expulsion
    for event, index in (('G', 0), ('C', 5), ('E', 6)):
        for item in lists[index].findall('li'):
            m = event_re.search(item.text)
            name, flag, when = m.groups()
            events.append({
                'MatchID': match['MatchID'],
                'PlayerID': player_id(match, name),
                'Country': flag,
                'Event': event,
                'Time': when,
            })

    # PG = Penalty goal, PS = Penalty save. P? = Penalty what happened?
    if len(lists) >= 9:
        for item in lists[8].findall('li'):
            name = pso_re.match(item.find('.//div[@class="name"]').text)
            name = name.group(1)
            event = item.find('.//div[@class="conclusion"]').text
            events.append({
                'MatchID': match['MatchID'],
                'PlayerID': player_id(match, name),
                'Event': 'P' + (event[0] if event is not None else '?'),
                'Country': '',
                'Time': 'PSO',
            })

    # Team and substitutions
    for i, ul in enumerate(lists[1:6]):
        name = ul.xpath('.//div[contains(@class, "bold")]')[0].text.strip()
        team = [li.text for li in ul.findall('li//span')]

matches_frame = pd.DataFrame(matches)

# 1950: Urugual - Brazil match is the "Final". Sweden - Spain is the Third place. Group F is Quarter-Finals
matches_frame.Stage[matches_frame.MatchID == '1206'] = 'Third place'
matches_frame.Stage[matches_frame.MatchID == '1190'] = 'Final'
matches_frame.Stage[matches_frame.MatchID == '1231'] = 'Quarter-finals'
matches_frame.Stage[matches_frame.MatchID == '1186'] = 'Quarter-finals'
matches_frame.Stage[matches_frame.MatchID == '1189'] = 'Quarter-finals'
matches_frame.Stage[matches_frame.MatchID == '1207'] = 'Quarter-finals'
        
matches_frame.to_csv('fifa-match.csv', encoding='utf-8', index=False)

teams.to_csv('fifa-team.csv', encoding='utf-8', index=False, cols=['Edition', 'Country', 'ID'] + team_cols)

# Sort by MatchID (int) and Time (PSO->null)
events_frame = pd.DataFrame(events)
events_frame['MatchID'] = events_frame['MatchID'].astype(int)
events_frame['Time'] = events_frame['Time'].str.replace("'", '').replace('PSO', pd.np.nan).astype(float)
event_cols = ['MatchID', 'Time', 'Event', 'PlayerID', 'Country']
events_frame.sort(event_cols, ascending=[False, True, True, True, True], inplace=True)
events_frame.to_csv('fifa-event.csv', encoding='utf-8', index=False, cols=event_cols, float_format='%.0f')

# TODO: save team_name
