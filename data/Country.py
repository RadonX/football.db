#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
# [正则表达式和python的re模块 | de部落格^_^](http://zhonghuan.info/2014/09/04/正则表达式和python的re模块)
# [Debuggex: Online visual regex tester](https://www.debuggex.com/)


def parse_fifa_code(inputpath, outputpath):

    # “@openfootball/national-teams“ is not very machine friendly

    inputfile = open(inputpath,encoding='UTF-8')
    outputfile = open(outputpath,'wt',encoding='UTF-8')
    #renull = re.compile('\s*(#|\n)?')
    recode = re.compile('\s*(\w{3}),(.+?)(\s{2,}|\n|#)') # ARG,Argentina

    #headline
    outputfile.write('ccode,country\n')
    #special examples
    outputfile.write('CIV,Ivory Coast\n')
    outputfile.write('TPE,Chinese Tapei\n')
    outputfile.write('VIR,United States Virgin Islands\n')
    outputfile.write('VIR,Virgin Islands (U.S.)\n')
    outputfile.write('BIH,Bosnia and Herzegovina\n')#Bosnia-Herzegovina
    outputfile.write('COD,DR Congo\n')#Congo DR
    outputfile.write('TPE,Chinese Taipei\n')#Taiwan
    outputfile.write('SCG,Serbia and Montenegro\n')


    count = 0
    while True:
        count += 1
        line = inputfile.readline()
        if line == "": break

        #delete \w
        result = recode.match(line)
        if result:
            try:
                country = result.group(2)
                if country[0] == ' ': country = country[1:]
                if country[-1] == ' ': country = country[:-1]
                outputfile.write(result.group(1)+','+country+'\n')
            except:
                print('err',line)


    print(count)
    inputfile.close()
    outputfile.close()

if __name__ == "__main__":
    parse_fifa_code("openmundi/world.db/1-codes/fifa.txt","fifacode.csv")
