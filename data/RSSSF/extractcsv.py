#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import csv


wcdocfile = open('data/World Cup 2014.txt')
#wcdoc = wcdocfile.read()

#matches = re.findall("(<(\d{4,5})>)?")
rematchinfo = re.compile('([0-9]{2})/([0-9]{2})/([0-9]{4})' )
reattendance = re.compile(', ([0-9]+)')
respace = re.compile('(\D+)\s+(\d+)')
renospace = re.compile('\S\w*\S')

mdate = [] #make it "mdate"!!!
attendance = []
ctype = []
home = []
away = []
score = []
scoreinfo = []
lineuphome = []
lineupaway = []
squadhome = []
squadaway = []
foul = []

line = wcdocfile.readline()
count = 0
while True:
    if not line:
        break
    date = rematchinfo.match(line)
    if date: #find one match
        count += 1

        mdate.append(date.group(3)+'-'+date.group(2)+'-'+date.group(1))
        attendance.append(reattendance.search(line,10).group(1))

        line = wcdocfile.readline()
        if(line[:-1] == 'World Cup'):
            ctype.append('WC')

        line = wcdocfile.readline()
        foo = respace.match(line)
        home.append( renospace.match(foo.group(1)).group() )
        scorehome = foo.group(2)
        scoreinfo_ = ''
        if (scorehome != '0'):
            scoreinfo_ += line[ line.find('(')+1: -1]
            if scoreinfo_.find(')') == -1:
                line = wcdocfile.readline().replace('  ','')
                scoreinfo_ += line[:-1]

        line = wcdocfile.readline()
        foo = respace.match(line)
        away.append( renospace.match(foo.group(1)).group() )
        scoreaway = foo.group(2)
        scoreinfo_ += ' - '
        if (scoreaway != '0'):
            scoreinfo_ += line[ line.find('(')+1: -1]
            if scoreinfo_.find(')') == -1:
                line = wcdocfile.readline().replace('  ','')
                scoreinfo_ += line[:-1]

        scoreinfo.append(scoreinfo_)
        '''
        if (scorehome == scoreaway):
            line = wcdocfile.readline()
            #blabla
        '''
        line = wcdocfile.readline()
        if (line.find('extra') != -1):
            line = wcdocfile.readline()
        score.append(scorehome+'-'+scoreaway )

        foo = respace.match(line)
        squad_ = foo.group(2)
        line = wcdocfile.readline()
        squad_ += line[line.find(' '):-1].replace('  ','')
        lineuphome.append(line[1:line.find(' ')-1])
        while True:
            line = wcdocfile.readline()
            if(line[0] != ' '):
                break
            squad_ += line.replace('  ','')
        squadhome.append(squad_.replace('\n',' '))

        foo = respace.match(line)
        try:
            squad_ = foo.group(2)
        except:
            print(line)
            #只能有一行进球,不然会出奇怪的错
        line = wcdocfile.readline()
        squad_ += line[line.find(' '):-1].replace('  ','')
        lineupaway.append(line[1:line.find(' ')-1])
        while True:
            line = wcdocfile.readline()
            if(line[0] != ' '):
                break
            squad_ += line.replace('  ','')
        squadaway.append(squad_.replace('\n',' '))
    else:
        if (line.find('Cards') != -1):
            foul_ = line[line.find(':',6)+1:-2] + ' - ' #respace cannot match ':'
            line = wcdocfile.readline()
            foul_ += line[line.find(':')+1:-2]
            foul.append(foul_)
        line = wcdocfile.readline()

wcdocfile.close()


#import sys
#f = open(sys.argv[1], 'wt')


f = open('data/match2014WC.csv','wt')
try:
    writer = csv.writer(f)
    #writer.writerow( ('mdate','attendance','ctype','home','away','score',
    #                  'scoreinfo','lineup','squadhome','squadaway','foul') )
    writer.writerow( ('mdate','attendance','ctype','home','away','score',
                      'lineuphome','lineupaway','cyear') )
    for i in range(count):
        writer.writerow((mdate[i], attendance[i], ctype[i], home[i], away[i], score[i],
                        lineuphome[i],lineupaway[i],2014))
except:
    print(mdate[i])
finally:
    f.close()

print(count)